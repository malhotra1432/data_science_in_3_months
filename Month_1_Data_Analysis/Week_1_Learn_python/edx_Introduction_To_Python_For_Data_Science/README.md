# Microsoft:DAT208x  
# Introduction To Python For Data Science
* EdX https://www.edx.org/course/introduction-python-data-science-2
---------------------------------------
# What will you learn?   
* Python    
* Specifically for Data Science   
* Store data  
* Manipulate data  
* Tools for data analysis
--------------------------------------  
# Python Basics    
     1. Lecture: Hello Python 
* Guido Van Rossum
* General Purpose: build anything  
* Open Source! Free!
* Python Packages, also for Data Science  
* Many applications and fields  
* Version 3.x - https://www.python.org/downloads/   

       Knowledge Checks   
* Which of the following statements is correct?  
  * The IPython Shell is typically used to work with Python interactively.  

* Which file extension is used for Python scripts files?
  * .py    

* You need to print the result of adding 3 and 4 inside a script. Which line of code should you write in the script?  
  * print(3 + 4)   
  
        Exercise                        
* Python is perfectly suited to do basic calculations. Apart from addition, subtraction, multiplication and division, there is also support for more advanced operations such as:
  
      Exponentiation: **. This operator raises the number to its left to the power of the number to its right: for example 4**2 will give 16.
      Modulo: %. It returns the remainder of the division of the number to the left by the number on its right, for example 18 % 7 equals 4.
  
  The code in the script on the right gives some examples.
  Instructions                          
  * print(100 * (1.1 ** 7))  
----------------------------------------  
    2. Lecture:Variables and Types  => Knowledge Check  
* Which line of code creates a variable x with the value 15?
  * x = 15

* What is the value of variable z after executing these commands?  
  x = 5  
  y = 7  
  z = x + y + 1
  * 13
  
* You  execute the following lines of python code:  
  x = "test"  
  y = false  
  Which of the following statement is correct?  
  * x is a string, y is a boolean.  
  
          Exercise  
* Create a variable savings and initialize the value with 100 and print it.
  * savings  = 100  
  print(savings)
  
* Create a variable factor, equal to 1.10.
* Use savings and factor to calculate the amount of money you end up with after 7 years. Store the result in a new variable , result.  
* Print the value of result.  
  * savings = 100  
  factor    = 1.10  
  result = savings * (factor ** 7)  
  print(savings)   
  
* Create a new string , desc, with the value "compound interest"  
* create a new boolean , profitable, with the value True.  
  * desc =   "compound interest"  
  profitable = True  

* Guess the type  
  * a is float, b is str, c is bool  
  
* Operations with other types   
* calculate the product of savings and factor. Store the result in year1.  
* What do you think the resulting type will be ? Find out by printing out the type of year1.  
* calculate sum of desc and desc and store the result in a new variable doubledesc.  
* Print out doubledesc. Did you expect this?   
  * type(year1)   
  print(desc + desc)  
 
* Hit Submit Answer to run the code on the right. Try to understand the error message.
* Fix the code on the right such that the printout runs without errors; use the function str() to convert the variables to strings.
* Convert the variable pi_string to a float and store this float as a new variable, pi_float.
   * savings = 100  
   result = 100 * 1.10 ** 7  
     print("I started with $" + savings + " and now have $" + result + ". Awesome!")  
     pi_string = "3.1415926"  
     pi_float = float(pi_string)  

         2.1 Lecture: Lists => Knowledge Checks
* Which of the following is a characteristic of a Python list?
  * It is a way to name a collection of values, instead of having to create separate variables for each element.   

* You use type(fam) and type(fam2) to reveal the type of a Python list. What is the result?  
  * fam = ["liz",1.73,"emma",1.68,"mom",1.71,"dad","1.89"]  
  * fam2 = [["liz",1.73],["emma",1.68],["mom",1.71],["dad",1.89]]  
  * type(fam) = list , type(fam2) = list  
  
* Which command is invalid Python syntax to create a list x?  
  * x = ["this" + "is", 1, True, "list"]  
  
* Which three Python data types does this list contain?  
  x = ["you", 2, "are", "so", True]  
  * int, str, bool  
       
        Exercise                                 
* Create a list, areas, that contains the area of the hallway (hall), kitchen (kit), living room (liv), bedroom (bed) and bathroom (bath), in this order. Use the predefined variables.
* Print areas with the print() function.  
  * areas = [hall,kit,liv,bed,bath]  
  
* Finish the line of code that creates the areas list such that the list first contains the name of each room as a string, and then its area. More specifically, add the strings "hallway", "kitchen" and "bedroom" at the appropriate locations.
* Print areas again; is the printout more informative this time?   
  * areas = ["hallway",float(hall),"kitchen",float(kit),"living room",float(liv),"bedroom",float(bed), "bathroom",float(bath)]  
  
* Can you tell which ones of the following lines of Python code are valid ways to build a list?  
A. [1, 3, 4, 2]  
B. [[1, 2, 3], [4, 5, 7]]  
C. [1 + 2, "a" * 5, 3]    
   * A,B and C  
   
* Finish the list of lists so that it also contains the bedroom and bathroom data. Make sure you enter these in order!
* Print out house; does this way of structuring your data make more sense?
* Print out the type of house. Are you still dealing with a list?    
  * house = [["hallway", hall],
         ["bedroom",bed],
         ["bathroom",bath],
         ["kitchen", kit],
         ["living room", liv]]     
         print(house)  
         print(type(house))
      
        2.2 Subsetting Lists => Knowledge Checks
* Which pair of symbols do you need to do list subsetting in Python?  
  * Brackets: []  
  
* What Python command should you use to extract the element with index 1 from a Python list x? More specifically, given a list x = [4, 5, 6, 7], which command do you need to extract the number 5?  
  * x[1]  
  
* You have a list y, containing 5 elements:  
y = ["this", "is", "a", True, "test"]  
Which two Python commands correctly extract the boolean value from this list?     
  * y[3] , y[-2]  
     
* You want to slice a list x. The general syntax is:  
x[begin:end]  
You need to replace begin and end with indexes according to the slice you want to make.  
Which of the following statements is correct?    
  * The begin index is included in the slice, the end index is not. correct  
    
        Exercise        
* Print out the second element from the areas list, so 11.25.
* Subset and print out the last element of areas, being 9.50. Using a negative index makes sense here!
* Select the number representing the area of the living room and print it out.                  
  * print(areas[1])  
  print(areas[-1])  
  print(areas[5])   
  
* Using a combination of list subsetting and variable assignment, create a new variable, eat_sleep_area, that contains the sum of the area of the kitchen and the area of the bedroom.
* Print this new variable eat_sleep_area.    
  * eat_sleep_area = areas[3] + areas[-3]  
  print(eat_sleep_area)  
  
* Use slicing to create a list, downstairs, that contains the first 6 elements of areas.
* Do a similar thing to create a new variable, upstairs, that contains the last 4 elements of areas.
* Print both downstairs and upstairs using print().    
  * downstairs = areas[0:6]  
  upstairs = areas[6:10]  
  print(downstairs)  
  print(upstairs)    
  
* Use slicing to create the lists downstairs and upstairs again, but this time without using indexes if it's not necessary. Remember downstairs is the first 6 elements of areas and upstairs is the last 4 elements of areas.  
  * downstairs = areas[:6]  
  upstairs = areas[6:]  
  print(downstairs)  
  print(upstairs)   
  
* Subsetting lists of lists
You saw before that a Python list can contain practically anything; even other lists! To subset lists of lists, you can use the same technique as before: square brackets. Try out the commands in the following code sample in the IPython Shell:

x = [["a", "b", "c"],
     ["d", "e", "f"],
     ["g", "h", "i"]]
x[2][0]
x[2][:2]
x[2] results in a list, that you can subset again by adding additional square brackets.
* What will house[-1][1] return? house, the list of lists that you created before, is already defined for you in the workspace. You can experiment with it in the IPython Shell.     
  * house = [['hallway', 11.25],
 ['kitchen', 18.0],
 ['living room', 20.0],
 ['bedroom', 10.75],
 ['bathroom', 9.5]]  
  A float: The bathroom area  
  
        Manipulating Lists => Knowledge Checks 
* You have a list x that is defined as follows:  
x = ["a", "b", "b"]  
You need to change the second "b" (the third element) to "c".  
Which command should you use?  
  * x[2] = "c"   
  
* You have a list x that is defined as follows:  
x = ["a", "b", "c"]  
Which line of Python code do you need to add "d" at the end of the list x?   
  * x = x + ["d"]  
  
* You have a list x that is defined as follows:   
x = ["a", "b", "c", "d"]  
You decide to remove an element from it by using del:  
del(x[3])    
How does the list x look after this operation?     
   * ["a", "b", "c"]  
   
         Exercise  
* You did a miscalculation when determining the area of the bathroom; it's 10.50 square meters instead of 9.50. Can you make the changes?
* Make the areas list more trendy! Change "living room" to "chill zone".         
  * areas = ["hallway", 11.25, "kitchen", 18.0, "living room", 20.0, "bedroom", 10.75, "bathroom", 9.50]  
  areas[-1] = 10.50  
  areas[4] = "chill zone"  
  print(areas)       
  
* Use the + operator to paste the list ["poolhouse", 24.5] to the end of the areas list. Store the resulting list as areas_1.
* Further extend areas_1 by adding data on your garage. Add the string "garage" and float 15.45. Name the resulting list areas_2.    
  * areas = ["hallway", 11.25, "kitchen", 18.0, "chill zone", 20.0,
         "bedroom", 10.75, "bathroom", 10.50]  
         areas_1 = areas + ["poolhouse", 24.5]  
         areas_2 = areas_1 + ["garage", 15.45]   
         print(areas_2)  
         
* Finally, you can also remove elements from your list. You can do this with the del statement:  
x = ["a", "b", "c", "d"]  
del(x[1])  
Pay attention here: as soon as you remove an element from a list, the indexes of the elements that come after the deleted element all change!  
The updated and extended version of areas that you've built in the previous exercises is coded below. You can copy and paste this into the IPython Shell to play around with the result.    
areas = ["hallway", 11.25, "kitchen", 18.0,
        "chill zone", 20.0, "bedroom", 10.75,
         "bathroom", 10.50, "poolhouse", 24.5,
         "garage", 15.45]    
         There was a mistake! The amount you won with the lottery is not that big after all and it looks like the poolhouse isn't going to happen. You decide to remove the corresponding string and float from the areas list.  
         The ; sign is used to place commands on the same line. The following two code chunks are equivalent:  
         Which of the code chunks will do the job for us?
  * del(areas[-4:-2])  
  
* Change the second command, that creates the variable areas_copy, such that areas_copy is an explicit copy of areas
* Now, changes made to areas_copy shouldn't affect areas. Hit Submit Answer to check this.           
  * areas = [11.25, 18.0, 20.0, 10.75, 9.50]  
  areas_copy = areas[0:4]  
  areas_copy[0] = 5.0  
  print(areas)  
  
        3. Python Functions => Knowledge Check  
* What is a Python function?  
  * A piece of reusable Python code that solves a particular problem.   
  
* You have a list named x. To calculate the minimum value in this list, you use the min() function.  
Which Python command should you use?  
  * min(x)  
               
* What Python command opens up the documentation from inside the IPython Shell for the min function?  
  * help(min)  
                 
* The function round has two arguments. Select the two correct statements about these arguments.  
  * number is a required argument.  
  ndigits is an optional argument.  
  
        Exercise                   
* Use print() in combination with type() to print out the type of var1.
* Use len() to get the length of the list var1. Wrap it in a print() call to directly print it out.
* Use int() to convert var2 to an integer. Store the output as out2.        
   * var1 = [1, 2, 3, 4]  
   var2 = True  
   print(type(var1))  
   print(len(var1))  
   out2 = int(var2)        
   
* Possible Answers
* complex() takes exactly two arguments: real and [, imag].
* complex() takes two arguments: real and imag. Both these arguments are required.
* complex() takes two arguments: real and imag. real is a required argument, imag is an optional argument.
* complex() takes two arguments: real and imag. If you don't specify imag, it is set to 1 by Python.     
  * complex() takes two arguments: real and imag. real is a required argument, imag is an optional argument.  
  
* Use + to merge the contents of first and second into a new list: full.
* Call sorted() on full and specify the reverse argument to be True. Save the sorted list as full_sorted.
* Finish off by printing out full_sorted.  
  * first = [11.25, 18.0, 20.0]  
  second = [10.75, 9.50]  
  full = first + second  
  full_sorted = sorted(full,reverse=True)  
  print(full_sorted)
  
        Methods => Knowledge Checks 
* What is append() in Python?  
  * append() is a method, and therefore also a function. correct  
* You have a string x defined as follows:  
x = "monty python says hi!"  
Which Python command should you use to capitalize this string x?   
  * x.capitalize()  
* How does the list x look after you execute the following two commands?    
x = [4, 9, 5, 7]  
x.append(6)  
  * [4, 9, 5, 7, 6]  
  
        Exercise  
* Use the upper() method on room and store the result in room_up. Use the dot notation.
* Print out room and room_up. Did both change?
* Print out the number of o's on the variable room by calling count() on room and passing the letter "o" as an input to the method. We're talking about the variable room, not the word "room"!           
  * room = "poolhouse"  
  room_up = room.upper()   
  print(room) 
  print(room_up)  
  print(room.count("o"))  
  
* Use the index() method to get the index of the element in areas that is equal to 20.0. Print out this index.
* Call count() on areas to find out how many times 14.5 appears in the list. Again, simply print out this number.    
  * areas = [11.25, 18.0, 20.0, 10.75, 9.50]  
  print(areas.index(20.0))  
  print(areas.count(14.5))

* Use append() twice to add the size of the poolhouse and the garage again: 24.5 and 15.45, respectively. Make sure to add them in this order.
* Print out areas
* Use the reverse() method to reverse the order of the elements in areas.
* Print out areas once more.  
  * areas = [11.25, 18.0, 20.0, 10.75, 9.50]  
  areas.append(24.5)  
  areas.append(15.45)  
  print(areas)  
  areas.reverse()  
  print(areas)  
  
         Functions and Packages  => Knowledge Checks    
* Which of the following is a package for installation and maintenance system for Python?  
  * pip  
  
* Which statement is the most common way to invoke the import machinery?  
  * import  
    
* You import Numpy as foo as follows:  
import numpy as foo  
Which Python command that used the array() function from Numpy is valid if Numpy is imported as foo?  
  * foo.array([1, 2, 3])  
  
*  Review
Review Question 4
0.0/1.0 point (graded)
You want to use Numpy's array() function.  
You need to decide whether to import this function as follows:  
from numpy import array  
or by importing the entire numpy package:  
import numpy  
Select the two correct statements about these different import methods.       
   * importing numpy with array makes code confusing. Better we can just import numpy and use numpy.array().  
   
         Exercise    
* Import the math package. Now you can access the constant pi with math.pi.
* Calculate the circumference of the circle and store it in C.
* Calculate the area of the circle and store it in A.        
  * r = 0.43  
  import math  
  C = 0  
  C = 2 * math.pi * r  
  A = 0  
  A = math.pi * r ** 2  
  print("Circumference: " + str(C))  
  print("Area: " + str(A))     
  
* Perform a selective import from the math package where you only import the radians function.
* Calculate the distance travelled by the Moon over 12 degrees of its orbit. Assign the result to dist. You can calculate this as r∗ϕ, where r is the radius and ϕ is the angle in radians. To convert an angle in degrees to an angle in radians, use the radians() function, which you just imported.
* Print out dist.    
  * r = 192500  
  from math import radians  
  ϕ = 12  
  dist = r * radians(ϕ)   
  print(dist) 
 
* There are several ways to import packages and modules into Python. Depending on the import call, you'll have to use different Python code.  
Suppose you want to use the function inv(), which is in the linalg subpackage of the scipy package. You want to be able to use this function as follows:  
my_inv([[1,2], [3,4]])  
Which import statement will you need in order to run the above code without an error?   
  * from scipy.linalg import inv as my_inv  
  
        Numpy => Knowledge Checks
* Which Numpy function do you use to create an array?  
  * array()  
  
* Which two statements describe the advantage of Numpy Package over regular Python Lists?
  * The Numpy Package provides the array, a data type that can be used to do element-wise calculations.  
  Because Numpy arrays can only hold element of a single type, calculations on Numpy arrays can be carried out way faster than regular Python lists.  
  
* What is the resulting Numpy array z after executing the following lines of code    
import numpy as np  
x = np.array([1, 2, 3])  
y = np.array([3, 2, 1])  
z = x + y  
  * array([4, 4, 4])  

* What happens when you put an integer, a Boolean, and a string in the same Numpy array using the array() function?  
  * All array elements are converted to strings. correct  
  
* Import the numpy package as np, so that you can refer to numpy with np.
* Use np.array() to create a Numpy array from baseball. Name this array np_baseball.
* Print out the type of np_baseball to check that you got it right.                  
  * baseball = [180, 215, 210, 210, 188, 176, 209, 200]  
  import numpy as np  
  np_baseball = np.array(baseball)  
  print(type(np_baseball))  
  
* Create a Numpy array from height. Name this new array np_height.
* Print np_height.
* Multiply np_height with 0.0254 to convert all height measurements from inches to meters. Store the new values in a new array, np_height_m.
* Print out np_height_m and check if the output makes sense.    
  * import numpy as np  
  np_height = np.array(height)  
  print(np_height)  
  np_height_m = np_height * 0.0254  
  print(np_height_m)  
  
* Create a Numpy array from the weight list with the correct units. Multiply by 0.453592 to go from pounds to kilograms. Store the resulting Numpy array as np_weight_kg.
* Use np_height_m and np_weight_kg to calculate the BMI of each player. Use the following equation:    

    BMI=weight(kg)/height(m)2  
* Save the resulting numpy array as bmi.
* Print out bmi.    
  * import numpy as np  
  np_height_m = np.array(height) * 0.0254  
  np_weight_kg = np.array(weight) * 0.453592  
  bmi = np_weight_kg / np_height_m ** 2  
  print(bmi)  
  
* Create a boolean Numpy array: the element of the array should be True if the corresponding baseball player's BMI is below 21. You can use the < operator for this. Name the array light.
* Print the array light.
* Print out a Numpy array with the BMIs of all baseball players whose BMI is below 21. Use light inside square brackets to do a selection on the bmi array.
  * import numpy as np  
  np_height_m = np.array(height) * 0.0254  
  np_weight_kg = np.array(weight) * 0.453592  
  bmi = np_weight_kg / np_height_m ** 2  
  light = bmi < 21  
  print(light)  
  print(bmi[light])  
  
* As Filip explained before, Numpy is great to do vector arithmetic. If you compare its functionality with regular Python lists, however, some things have changed.  
First of all, Numpy arrays cannot contain elements with different types. If you try to build such a list, some of the elments' types are changed to end up with a homogenous list. This is known as type coercion.  
Second, the typical arithmetic operators, such as +, -, * and / have a different meaning for regular Python lists and Numpy arrays.  
Have a look at this line of code:  
np.array([True, 1, 2]) + np.array([3, 4, False])  
Can you tell which code chunk builds the exact same Python data structure? The Numpy package is already imported as np, so you can start experimenting in the IPython Shell straight away!    
  * np.array([4, 3, 0]) + np.array([0, 2, 2])

* Subset np_weight: print out the element at index 50.
* Print out a sub-array of np_height: It contains the elements at index 100 up to and including index 110  
  * import numpy as np  
  np_weight = np.array(weight)  
  np_height = np.array(height)  
  print(np_weight[50])  
  print(np_height[100:111])  

        2D Numpy Arrays =>  Knowledge Checks    
* What charaterizes multi-dimensional Numpy arrays?  
  * You can create a 2D Numpy array from a regular list of lists.   
  
* You created the following 2D Numpy array, x:    
import numpy as np  
x = np.array([["a", "b", "c", "d"],  
              ["e", "f", "g", "h"]])  
              Which Python command do you use to select the string "g" from x?            
  * x[1,2]  
  
* What does the resulting array z contain after executing the following lines of Python code?  
  import numpy as np  
x = np.array([[1, 2, 3],  
              [1, 2, 3]])  
y = np.array([[1, 1, 1],  
              [1, 2, 3]])  
z = x - y                
  * array([[0, 1, 2],  
       [0, 0, 0]])  
                 
        Exercise       
* Use np.array() to create a 2D Numpy array from baseball. Name it np_baseball.
* Print out the type of np_baseball.
* Print out the shape attribute of np_baseball. Use np_baseball.shape.       
  * baseball = [[180, 78.4],
            [215, 102.7],
            [210, 98.5],
            [188, 75.2]]  
            import numpy as np  
            np_baseball = np.array(baseball)    
            print(type(np_baseball))  
            print(np_baseball.shape)  
            
* Use np.array() to create a 2D Numpy array from baseball. Name it np_baseball.
* Print out the shape attribute of np_baseball.                   
  * import numpy as np  
  np_baseball = np.array(baseball)  
  print(np_baseball.shape)              
  
* Print out the 50th row of np_baseball.
* Make a new variable, np_weight, containing the entire second column of np_baseball.
* Select the height (first column) of the 124th baseball player in np_baseball and print it out.    
  * import numpy as np  
  np_baseball = np.array(baseball)  
  print(np_baseball[49,:])  
  np_weight  = np_baseball[:,1]  
  print(np_weight)  
  print(np_baseball[123,0])  
  
* You managed to get hold on the changes in weight, height and age of all baseball players. It is available as a 2D Numpy array, update. Add np_baseball and update and print out the result.
* You want to convert the units of height and weight. As a first step, create a Numpy array with three values: 0.0254, 0.453592 and 1. Name this array conversion.
* Multiply np_baseball with conversion and print out the result.    
  * import numpy as np  
  np_baseball = np.array(baseball)  
  print(np_baseball + update)  
  conversion = np.array([0.0254, 0.453592, 1])  
  print(np_baseball * conversion)  

        Basic Statistics with Numpy   Knowledge Checks  
* Which of the following statement about basic statistics with Numpy is correct?
* Note: assume that the Numpy package is imported as np.          
  * Numpy offers many functions to calculate basic statistics, such as np.mean(), np.median() and np.std().   
  
* You are writing code to measure your travel time and weather conditions to work each day.
* The data is recorded in a Numpy array where each row specifies the measurements for a single day.
* The first column specifies the temperature in Fahrenheit. The second column specifies the amount of travel time in minutes.
* The following is a sample of the code.      
import numpy as np  
x = np.array([[28, 18],  
              [34, 14],  
              [32, 16],  
              ...  
              [26, 23],  
              [23, 17]])  
* Which Python command do you use to calculate the average travel time?
  * np.mean(x[:,1])  
  
* As a wrap up, have a look at the statements below about Numpy in general. Select the three statements that hold.  
  * Numpy is a great alternative to the regular Python list if you want to do Data Science in Python.  
  Numpy arrays can only hold elements of the same basic type.  
  Next to an efficient data structure, Numpy also offers tools to calculate summary statistics and to simulate statistical distributions.  

         Exercise 
* Create Numpy array np_height, that is equal to first column of np_baseball.
* Print out the mean of np_height.
* Print out the median of np_height.              
  * import numpy as np  
  np_height = np.array(np_baseball[:,0])  
  print(np.mean(np_height))  
  print(np.median(np_height))
  
* The code to print out the mean height is already included. Complete the code for the median height. Replace None with the correct code.
* Use np.std() on the first column of np_baseball to calculate stddev. Replace None with the correct code.
* Do big players tend to be heavier? Use np.corrcoef() to store the correlation between the first and second column of np_baseball in corr. Replace None with the correct code.    
  * import numpy as np  
  avg = np.mean(np_baseball[:,0])  
  print("Average: " + str(avg))  
  med = np.median(np_baseball[:,0])  
  print("Median: " + str(med))  
  stddev = np.std(np_baseball[:,0])  
  print("Standard Deviation: " + str(stddev))  
  corr = np.corrcoef(np_baseball[:,0],np_baseball[:,1])  
  print("Correlation: " + str(corr))  
  
* Convert heights and positions, which are regular lists, to numpy arrays. Call them np_heights and np_positions.
* Extract all the heights of the goalkeepers. You can use a little trick here: use np_positions == 'GK' as an index for np_heights. Assign the result to gk_heights.
* Extract all the heights of the all the other players. This time use np_positions != 'GK' as an index for np_heights. Assign the result to other_heights.
* Print out the median height of the goalkeepers using np.median(). Replace None with the correct code.
* Do the same for the other players. Print out their median height. Replace None with the correct code.    
  * np_positions = np.array(positions)    
np_heights = np.array(heights)  
gk_heights = np_heights[np_positions == 'GK']  
other_heights = np_heights[np_positions != 'GK']  
print("Median height of goalkeepers: " + str(np.median(gk_heights)))  
print("Median height of other players: " + str(np.median(other_heights)))  

        Plotting with Matplotlib  =>  Knowledge Checks  
* What is the characteristic about data visualization?  
  * Visualization is a very powerful tool for exploring your data and reporting results. correct  
  
* What is the conventional way of importing the pyplot sub-package from the matplotlib package?  
  * import matplotlib.pyplot as plt  
  
* You are creating a line plot using the following code:    
a = [1, 2, 3, 4]  
b = [3, 9, 2, 6]  
plt.plot(a, b)  
plt.show()  
Which two options describe the result of your code?            
  * The values in a are mapped onto the horizontal axis.  
  The values in b are mapped onto the vertical axis.  

* You are modifying the following code that calls the plot() function to create a line plot:    
a = [1, 2, 3, 4]  
b = [3, 9, 2, 6]  
plt.plot(a, b)  
plt.show()  
What should you change in the code to create a scatter plot instead of a line plot?    
  * Change plot() in plt.plot() to scatter().   
  
        Exercise  
* print() the last item from both the year and the pop list to see what the predicted population for the year 2100 is.
* Before you can start, you should import matplotlib.pyplot as plt. pyplot is a sub-package of matplotlib, hence the dot.
* Use plt.plot() to build a line plot. year should be mapped on the horizontal axis, pop on the vertical axis. Don't forget to finish off with the show() function to actually display the plot.            
  * print(year[-1],pop[-1])  
  import matplotlib.pyplot as plt   
  plt.plot(year,pop)  
  plt.show()

* Have another look at the plot you created in the previous exercise; it's shown on the right. What is the first year in which there will be more than ten billion human beings on this planet?  
  * 2062  
  
* Print the last item from both the list gdp_cap, and the list life_exp; it is information about Zimbabwe.
* Build a line chart, with gdp_cap on the x-axis, and life_exp on the y-axis. Does it make sense to plot this data on a line plot?
* Don't forget to finish off with a plt.show() command, to actually display the plot.    
  * print(gdp_cap[-1], life_exp[-1])  
  plt.plot(gdp_cap,life_exp)  
  plt.show()  
  
* Change the line plot that's coded in the script to a scatter plot.
* A correlation will become clear when you display the GDP per capita on a logarithmic scale. Add the line plt.xscale('log').
* Finish off your script with plt.show() to display the plot.    
   * plt.plot(gdp_cap, life_exp)  
plt.scatter(gdp_cap, life_exp)    
plt.xscale('log')  
plt.show()    

* Start from scratch: import matplotlib.pyplot as plt.
* Build a scatter plot, where pop is mapped on the horizontal axis, and life_exp is mapped on the vertical axis.
* Finish the script with plt.show() to actually display the plot. Do you see a correlation?
  * import matplotlib.pyplot as plt  
  plt.scatter(pop,life_exp)  
  plt.show()
 
        Histograms =>  Knowledge Checks  
* What is a characteristic of a histogram?     
  * Histogram is a great tool for getting a first impression about the distribution of your data. correct  
  
* You are working with a Python list with 10 different values. You divide the values into 5 equally-sized bins.  
* How wide will these bins be if the lowest value in your list is 0 and the highest is 20?       
  * 4  
  
* You write the following code:     
import matplotlib.pyplot as plt  
x = [1, 3, 6, 3, 2, 7, 3, 9, 7, 5, 2, 4]
plt.hist(x)  
plt.show()    
You need to extend the plt.hist() command to specifically set the number of bins to 4. What should you do?  
  * Add a second argument to plt.hist(): plt.hist(x, bins = 4)  
  
* Use plt.hist() to create a histogram of the values in life_exp. Do not specify the number of bins; Python will set the number of bins to 10 by default for you.
* Add plt.show() to actually display the histogram. Can you tell which bin contains the most observations?   
  * plt.hist(life_exp)  
plt.show()  

* Build a histogram of life_exp, with 5 bins. Can you tell which bin contains the most observations?
* Build another histogram of life_exp, this time with 20 bins. Is this better?  
  * plt.hist(life_exp,bins = 5)  
plt.show()  
plt.clf()  
plt.hist(life_exp,bins = 20)  
plt.show()
plt.clf()

* Build a histogram of life_exp with 15 bins.
* Build a histogram of life_exp1950, also with 15 bins. Is there a big difference with the histogram for the 2007 data?  
  * plt.hist(life_exp,bins = 15)  
plt.show()  
plt.clf()    
plt.hist(life_exp1950,bins=15)  
plt.show()  
plt.clf()  

* You're a professor teaching Data Science with Python, and you want to visually assess if the grades on your exam follow a normal distribution. Which plot do you use?  
  * Histogram  
  
* You're a professor in Data Analytics with Python, and you want to visually assess if longer answers on exam questions lead to higher grades. Which plot do you use?  
  * Scatter Plot  
  
        Customization =>  Knowledge Checks    
* You are customizing a plot by labelling its axes. You need to do this by using matplotlib.  
Which code should you use?        
   * xlabel("x-axis title") and ylabel("y-axis title") correct  

* Which matplotlib function do you use to build a line plot where the area under the graph is colored?
  * fill_between()
  
* Typically, you place all customization commands between the plot() call and the show() call, as follows:  
  import matplotlib.pyplot as plt  
x = [1, 2, 3]  
y = [4, 5, 6]  
plt.plot(x, y)  
plt.show()  
What will happen if you place the customization code after the show() function instead?  
  * Python doesn't throw an error, but you won't see your customizations. The show() function displays the plot you've built up until then. If the customizations come afterwards, there is no effect on the shown output. 
         
* The strings xlab and ylab are already set for you. Use these variables to set the label of the x- and y-axis.
* The string title is also coded for you. Use it to add a title to the plot.
* After these customizations, finish the script with plt.show() to actually display the plot.  
  * plt.scatter(gdp_cap, life_exp)  
  plt.xscale('log')   
xlab = 'GDP per Capita [in USD]'  
ylab = 'Life Expectancy [in years]'  
title = 'World Development in 2007'  
plt.xlabel(xlab)
plt.ylabel(ylab)  
plt.title(title)  
plt.show()       

* Use tick_val and tick_lab as inputs to the xticks() function to make the the plot more readable.
* As usual, display the plot with plt.show() after you've added the customizations.  
  * plt.scatter(gdp_cap, life_exp)  
  plt.xscale('log')   
  plt.xlabel('GDP per Capita [in USD]')  
  plt.ylabel('Life Expectancy [in years]')  
  plt.title('World Development in 2007')  
tick_val = [1000,10000,100000]  
tick_lab = ['1k','10k','100k']    
plt.xticks(tick_val,tick_lab)    
plt.show()
 
* Run the script to see how the plot changes.
* Looks good, but increasing the size of the bubbles will make things stand out more.
* Import the numpy package as np.
* Use np.array() to create a numpy array from the list pop. Call this Numpy array np_pop.
* Double the values in np_pop by assigning np_pop * 2 to np_pop again. Because np_pop is a Numpy array, each array element will be doubled.
* Change the s argument inside plt.scatter() to be np_pop instead of pop. 
  * import numpy as np  
np_pop = np.array(pop)   
np_pop = np_pop * 2  
plt.scatter(gdp_cap, life_exp, s = np_pop)  
plt.xscale('log')   
plt.xlabel('GDP per Capita [in USD]')  
plt.ylabel('Life Expectancy [in years]')  
plt.title('World Development in 2007')  
plt.xticks([1000, 10000, 100000],['1k', '10k', '100k'])  
plt.show()

* Add c = col to the arguments of the plt.scatter() function.
* Change the opacity of the bubbles by setting the alpha argument to 0.8 inside plt.scatter(). Alpha can be set from zero to one, where zero totally transparant, and one is not transparant.  
  * plt.scatter(x = gdp_cap, y = life_exp,c = col,alpha = 0.8, s = np.array(pop) * 2)  
plt.xscale('log')   
plt.xlabel('GDP per Capita [in USD]')  
plt.ylabel('Life Expectancy [in years]')  
plt.title('World Development in 2007')  
plt.xticks([1000,10000,100000], ['1k','10k','100k'])  
plt.show()

* Add plt.grid(True) after the plt.text() calls so that gridlines are drawn on the plot.  
  * plt.scatter(x = gdp_cap, y = life_exp, s = np.array(pop) * 2, c = col, alpha = 0.8)  
plt.xscale('log')   
plt.xlabel('GDP per Capita [in USD]')  
plt.ylabel('Life Expectancy [in years]')  
plt.title('World Development in 2007')  
plt.xticks([1000,10000,100000], ['1k','10k','100k'])  
plt.text(1550, 71, 'India')  
plt.text(5700, 80, 'China')  
plt.grid(True)  
plt.show()  

* If you have a look at your colorful plot, it's clear that people live longer in countries with a higher GDP per capita. No high income countries have really short life expectancy, and no low income countries have very long life expectancy. Still, there is a huge difference in life expectancy between countries on the same income level. Most people live in middle income countries where difference in lifespan is huge between countries; depending on how income is distributed and how it is used.
* What can you say about the plot?  
  * 
The countries in blue, corresponding to Africa, have both low life expectancy and a low GDP per capita.  

       Boolean Logic and Control Flow =>  Knowledge Checks  
* What is the result of the following comparison?  
  5 >= 5
  * True  
  
* What is the result of the following comparison?  
4 != 4              
   * False
   
* What is the characteristic of the and operator?  
  * The and operator returns True only if both operands are True.  
  
* What is the characteristic of the or operator?  
  * The or operator returns True if at least one of the operands is True.  
  
* You write the following code:  
x = 5  
if x > 6 :  
    print("high")  
else :  
    print("low")   
What will be printed out if you execute the code?      
  * low  
  
* You write the following code:   
x = 7  
if x > 6 :  
    print("high")  
elif x > 3 :  
    print("ok")  
    else :  
    print("low")    
What will be printed out if you execute the code?  
  * high  
  
         Exercise  
* In the editor on the right, write code to see if True equals False.
* Write Python code to check if -5 * 15 is not equal to 75.
Ask Python whether the strings "pyscript" and "PyScript" are equal.
What happens if you compare booleans and integers? Write code to see if True and 1 are equal.  
  * print(True == False)  
print(-5 * 15 != 75)  
print("pyscript" == "PyScript")  
print(True  == 1)  

* Write Python expressions, wrapped in a print() function, to check whether:
* x is greater than or equal to -10. x has already been defined for you.
* "test" is less than or equal to y. y has already been deinfed for you.
* True is greater than False.  
  * x = -3 * 6  
print(x >= -10)  
y = "test"  
print("test" <= y)  
print(True > False)  

* Write Python expressions, wrapped in a print() function, to check whether:  
* my_kitchen is bigger than 10 and smaller than 18.
* my_kitchen is smaller than 14 or bigger than 17.
* double the area of my_kitchen is smaller than triple the area of your_kitchen.  
  * my_kitchen = 18.0  
your_kitchen = 14.0  
print(my_kitchen > 10 and my_kitchen < 18)  
print(my_kitchen < 14 or my_kitchen > 17)  
print(my_kitchen * 2 < your_kitchen * 3)  

* and, or, not (2)  
To see if you completely understood the boolean operators, have a look at the following piece of Python code:  
x = 8  
y = 9  
not(not(x < 3) and not(y > 14 or y > 10))  
What will the result be if you exectue these three commands in the IPython Shell?  
NB: Notice that not has a higher priority than and and or, it is executed first.  
  * False  
  
* To experiment with if and else a bit, have a look at this code sample:  
area = 10.0  
if(area < 9) :  
    print("small")  
elif(area < 12) :  
    print("medium")  
else :  
    print("large")  
What will the output be if you run this piece of code in the IPython Shell?    
  * medium 
  
* Examine the if statement that prints out "Looking around in the kitchen." if room equals "kit".
* Write another if statement that prints out "big place!" if area is greater than 15.  
  * room = "kit"  
area = 14.0  
if room == "kit" :  
    print("looking around in the kitchen.")  
if(area > 15):  
    print("big place!")  
    
* Add an else statement to the second control structure so that "pretty small." is printed out if area > 15 evaluates to False.  
  * room = "kit"  
area = 14.0    
if room == "kit" :  
    print("looking around in the kitchen.")  
else :  
    print("looking around elsewhere.")    
if area > 15 :  
    print("big place!")    
room = "kit"
area = 14.0  
if room == "kit" :  
    print("looking around in the kitchen.")  
else :  
    print("looking around elsewhere.")    
if area > 15 :  
    print("big place!")  
else:  
    print("pretty small.")   
    
* Add an elif to the second control structure such that "medium size, nice!" is printed out if area is greater than 10.  
  * room = "bed"  
area = 14.0  
if room == "kit" :  
    print("looking around in the kitchen.")  
elif room == "bed":  
    print("looking around in the bedroom.")  
else :   
    print("looking around elsewhere.")  
if area > 15 :  
    print("big place!")  
elif area > 10:  
   print("medium size, nice!")       
else :  
    print("pretty small.")  
    
        Pandas  =>  Knowledge Checks        
 * How is a Pandas DataFrame different from a 2D Numpy array?  
   * In Pandas, different columns can contain different types. 
   
* What are two characteristics that describe Pandas DataFrame?
   * The rows correspond to observations.  
   The columns correspond to variables.  
   
* Which Pandas function do you use to import data from a comma-separated value (CSV) file into a Pandas DataFrame?  
  * read_csv()  
  
* Which technique should you use to select an entire row by its row label when accessing data in a Pandas DataFrame?  
  * loc  
  
        Exercise  
* Import pandas as pd.
* Read in the data from recent_grads_url (which is a CSV file) and assign it to a variable called recent_grads.
* Print the shape of recent_grads.                  
  * import pandas as pd  
recent_grads = pd.read_csv(recent_grads_url)  
print(recent_grads.shape)

* Print the .dtypes of your data so that you know what each column contains.
* Output basic summary statistics using a single pandas function.
* With the same function from before, summary statistics for all columns that aren't of type object.  
  * print(recent_grads.dtypes)    
print(recent_grads.describe())  
print(recent_grads.describe(exclude=["object"]))

* Look at the dtypes of the columns in columns to make sure that the data is numeric.
* It looks like a string is being used to encode missing values. Use the .unique() method to figure out what the string is.
* Search for missing values in the median, p25th, and p75th columns.
* Replace the found missing values with a NaN value, using numpy's np.nan.  
  * columns = ['median', 'p25th', 'p75th']
print(recent_grads[columns].dtypes)
print(recent_grads["median"].unique())
for column in columns:
    recent_grads.loc[recent_grads[column] == 'UN', column] = np.nan  
    
* Select the sharewomen column from recent_grads and assign this to a variable named sw_col.
* Output the first 5 rows of sw_col.      
  * sw_col = recent_grads['sharewomen']  
print(sw_col[0:5])  

* Import numpy as np.
* Using a numpy built-in function, find the maximum value of the sharewomen column and assign this value to the variable max_sw.
* Print the value of max_sw  
  * import numpy as np  
  max_sw = np.max(sw_col)  
  print(max_sw)  
  
* Output the row of data for the department that has the largest percentage of women.  
  * print(recent_grads[sw_col == max_sw])  
  
* Select the columns unemployed and low_wage_jobs from recent_grads, then convert them to a numpy array. Save this as recent_grads_np.
* Print the type of recent_grads_np to see that it is a numpy array.
  * recent_grads_np = recent_grads[["unemployed", "low_wage_jobs"]].values  
  print(type(recent_grads_np))  
  
* Calculate the correlation matrix of the numpy array recent_grads_np.  
  * print(np.corrcoef(recent_grads_np[:,0], recent_grads_np[:,1]))  
  
* Create a new column named sharemen, that contains the percentage of men for a given department by dividing the number of men by the total number of students for each department.
  * recent_grads['sharemen'] = recent_grads['men'] / recent_grads['total']        

* Using numpy, find the maximum value for the percentage of men and call this variable max_men.
* Select the row that has the percentage of men which corresponds to max_men.  
  * max_men = np.max(recent_grads['sharemen'])
print(recent_grads[recent_grads['sharemen'] == max_men])  
 
* Add a column named gender_diff that reports how much higher the rate of women is than the rate of men. 
   * recent_grads['gender_diff'] = recent_grads['sharewomen'] - recent_grads['sharemen']  
   
* Use numpy and pandas to convert each value in the gender_diff column to its absolute value.
* Output the five departments with the most balanced gender ratios.     
  * recent_grads['gender_diff'] = np.abs(recent_grads['gender_diff'])
print(recent_grads.nsmallest(5, 'gender_diff'))  

* Create diff_30, a boolean Series that is True when the corresponding value of gender_diff is greater than 0.30 and False otherwise.
* Make another boolean Series called more_men that's true when the corresponding row in recent_grads has more men than women.
* Combine your two Series to make one that you can use to select rows that have both more men than women and a value of gender_diff greater than 0.30. Save the result as more_men_and_diff_30.
* Use this new boolean Series to create the DataFrame fewer_women that contains only the rows you're interested in.
  *  more_men = recent_grads['sharemen'] > recent_grads['sharewomen']    
more_men_and_diff_30 = np.logical_and(more_men, diff_30)    
fewer_women = recent_grads[more_men_and_diff_30]    

* Using the .groupby() method, group the recent_grads DataFrame by 'major_category' and then count the number of departments per category using .count().  
  * print(recent_grads.groupby(['major_category']).major_category.count())  
  
* Create a DataFrame that groups the departments by major category and shows the number of departments that are skewed in women.  
  * print(fewer_women.groupby(['major_category']).major_category.count())  
  
* Write code that outputs the average gender percentage difference by major category.  
  * print(recent_grads.groupby(['major_category']).gender_diff.mean())  
  
* Write a query that outputs the mean number of 'low_wage_jobs' and average 'unemployment_rate' grouped by 'major_category'.  
  * dept_stats = recent_grads.groupby(['major_category'])['low_wage_jobs', 'unemployment_rate'].mean()
print(dept_stats)  

* Import matplotlib.pyplot with the alias plt.
* Create a scatter plot between unemployment_rate and low_wage_jobs per major category.
* Label the x axis with 'Unemployment rate'.
* Label the y axis with 'Low pay jobs'.    
  * import matplotlib.pyplot as plt  
plt.scatter(unemployment_rate,low_wage_jobs)  
plt.xlabel('Unemployment rate')  
plt.ylabel('Low pay jobs')  
plt.show()

* Create the scatterplot visualization between the unemployment rate and number of low wage jobs per major category using the .plot() method.
* Customize this scatterplot so that the points are red triangles by setting the color argument to "r" and the marker argument ^.  
* Display the plot you've created!
  * plt.scatter(unemployment_rate, low_wage_jobs, color = "r", marker = "^")  
plt.show()  

* Use matplotlib to create a histogram of sharewomen.
* Show the plot you created.  
  * plt.hist(sharewomen)  
plt.show()

* Use the .plot() method with kind='scatter' on the dept_stats DataFrame to create a scatter plot with 'unemployment_rate' on the x-axis and 'low_wage_jobs' on the y-axis. 
  * import matplotlib.pyplot as plt
import pandas as pd  
dept_stats.plot(kind='scatter', x='unemployment_rate', y='low_wage_jobs')
plt.show()  

* Now, create a histogram of the sharewomen column of the recent_grads DataFrame.  
  * import matplotlib.pyplot as plt  
import pandas as pd  
recent_grads.sharewomen.plot(kind='hist')  
plt.show()  

* First, create a DataFrame to plot. Use recent_grads to make a DataFrame that reports each major category and the number of college graduates with a job that doesn't require a degree. Assign this to a variable named df.
* Plot this data as a bar chart using the .plot() method. Here, kind should be "bar". 
* Display the plot you've created!   
  * df = recent_grads.groupby(['major_category']).non_college_jobs.sum()    
df.plot(kind = 'bar')  
plt.show()

* Use pandas to create a DataFrame that reports the number of graduates working at jobs that do require college degrees ('college_jobs'), and do not require college degrees ('non_college_jobs'). Assign this to a variable named df1.  
* Create a plot that reports this data with matplotlib.
* Display the plot you've created!
  *  df1 = recent_grads.groupby(['major_category'])['college_jobs','non_college_jobs'].sum()
df1.plot(kind="bar")
plt.show()  

* With a single line of code, drop all the rows that have a missing value.
* Print the size of the manipulated DataFrame.
  * print(recent_grads.size)
recent_grads.dropna(axis = 0, inplace = True)
print(recent_grads.size)  

* The columns median, p25th, and p75th are currently encoded as strings. Convert these columns to numerical values. Then, divide the value of each column by 1000 to make the scale of the final plot easier to read.
* Find the of each of the three columns for each major category. Save this as sal_quantiles  
  * recent_grads['median'] = pd.to_numeric(recent_grads['median']) / 1000  
recent_grads['p25th'] = pd.to_numeric(recent_grads['p25th']) / 1000  
recent_grads['p75th'] = pd.to_numeric(recent_grads['p75th']) / 1000  
columns = ['median', 'p25th', 'p75th']  
sal_quantiles = recent_grads.groupby(['major_category'])[columns].mean()  

* Plot the DataFrame.
* Clean up the x-axis labels with the function plt.xticks(). Set the first argument equal to np.arange(len(sal_quantiles.index)), the second argument equal to sal_quantiles.index, and the keyword argument rotation = 'vertical'.
* Show the plot.
* Now call the .plot() method with the argument subplots=True to plot the columns on separate axes. Show this plot as well.  
  * sal_quantiles.plot()    
plt.xticks(  
    np.arange(len(sal_quantiles.index)),  
    sal_quantiles.index,   
    rotation='vertical')      
plt.show()    
sal_quantiles.plot(subplots=True)
plt.show()  
